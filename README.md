This repo is for any maurices email assets.

Currently, you can find the shell file and the sample components here.

Any updates set to go live in Responsys should be reflected here for reference.
(or else...)



# Email Coding Wiki

## General Issues/Fixes

**Button Hover States**
_`:hover` is not supported inline (most clients)._
  
> **FIX:** Place `:hover` declarations in `<head>`.

**Anchor Tag Underline**
_`text-decoration: none;` is not supported in `<head>` (most clients)._
  
> **FIX:** Place an extra `text-decoration: none;` inline.


## Client Specific Issues/Fixes

**Various versions of Outlook 2013, 2010**
_Asterisk adjusts `line-height` for entire `<p>` tag. This results in clipped text._
  
> **FIX:** Add `margin: 0; line-height: normal;` to the inline style for the `<span>` tag containing the asterisk.

**Yahoo Clients**
_First CSS line in `<head>` is not read after a new, empty line of CSS._ 

> **FIX:** Add a blank class before new set of classes. Ex; `.trash{}`

**Various versions of iPhone**
_Width of all elements are sized down._
  
> **FIX:** Add `<meta name="x-apple-disable-message-reformatting" />` to the `<head>` of the email.

**Various versions of Outlook 2013, 2010**
_`max-width` is not supported._
  
> **FIX:** Add `width="600"` to any `<td>` tags along with adding the following within the main `<td>` tag:
`<!--[if (gte mso 9)|(IE)]><table align="center" cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr><td align="center" valign="top" width="600"><![endif]-->`
Then close these outlook tables with:
`<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->`

**Lotus Notes**
_`background-image`, `background-color` are not supported._
  
> **FIX:** Add `bgcolor="#hex"` to the table as a fallback.
